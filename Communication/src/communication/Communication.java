package communication;

import java.io.IOException;

public interface Communication {
	public void openBluetoothConnection() throws IOException;

	public void closeBluetoothConnection();

	public boolean isRobotReady();

	public void sendToRobot(byte[] command) throws IOException,
			IllegalArgumentException;

	public boolean hasConnection();

	public int receiveByteFromRobot() throws IOException;
}
