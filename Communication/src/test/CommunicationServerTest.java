package test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;


import communication.Constants;
import communication.CommunicationServer;

public class CommunicationServerTest implements Runnable {

	private static final int PORT = 6789;

	public void run() {
		startServer();
	}

	// starts the communication server
	private static void startServer() {
		new CommunicationServer();
	}

	// starts a client which then sends some test commands to our server
	private static void startClient() {
		Socket socket = null;

		try {
			socket = new Socket("localhost", PORT);
		} catch (IOException e) {
			System.err.println("Accept failed: " + PORT);
		}

		if (socket != null) {
			try {
	            OutputStream out = socket.getOutputStream();
	            
	            byte[] command = ByteBuffer.allocate(Constants.COMMAND_SIZE).array();
	            command[0] = Constants.STOP;
	            command[1] = 100;
	            command[2] = -100;
	            
	            out.write(command);
            } 
			catch (IOException e) {
            	System.err.println("Couldn't write to socket");
            }
		}
	}

	public static void main(String args[]) throws InterruptedException {

		// this starts our communication server in a separate thread
		new Thread(new CommunicationServerTest()).start();

		Thread.sleep(5000);

		// now we can start the test client
		startClient();

	}
}
